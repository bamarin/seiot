#define LED_PIN 9

int brightness;
int fadeAmount;
int currIntensity;

void setup() {
  // put your setup code here, to run once:
  currIntensity = 0;
  fadeAmount = 5;
  pinMode(LED_PIN, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  //digitalWrite(LED_PIN, HIGH);
  //Serial.println("ON");
  //delay(1000);
  //digitalWrite(LED_PIN, LOW);
  //Serial.println("OFF");
  //delay(1000);
  analogWrite(LED_PIN, currIntensity);
  currIntensity += fadeAmount;
  if (currIntensity <=0 || currIntensity >= 255){
    fadeAmount = -fadeAmount;
  }
  delay(20);
}
