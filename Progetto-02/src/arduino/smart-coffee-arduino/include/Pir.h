/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __PIR__
#define __PIR__

#include "Arduino.h"

class Pir {

public:
  Pir(const int pin);
  bool isMoved();

private:
  int pin;
};

#endif /* end of include guard: __PIR__ */
