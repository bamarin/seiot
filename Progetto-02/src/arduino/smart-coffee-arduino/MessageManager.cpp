/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/MessageManager.h"

MessageManager::MessageManager() {
  Serial.begin(9600);
}

MessageManager& MessageManager::getInstance() {
  static MessageManager messageManager;
  return messageManager;
}

void MessageManager::sendMessage(String message) {
  String payload = String(MESSAGE);
  payload.concat(message);
  payload.concat("\n");
  Serial.print(payload);
}

void MessageManager::sendSugar(int quantity) {
  String payload = String(SUGAR);
  payload.concat(String(quantity));
  payload.concat("\n");
  Serial.print(payload);
}

void MessageManager::sendProgress(int progress) {
  String payload = String(PROGRESS);
  payload.concat(String(progress));
  payload.concat("\n");
  Serial.print(payload);
}

void MessageManager::setMaintenance(int code) {
  String payload = String(MAINTENANCE);
  payload.concat("\n");
  Serial.print(payload);
}

void MessageManager::log(String messageLog) {
  String payload = String(LOG);
  payload.concat(messageLog);
  payload.concat("\n");
  Serial.print(payload);
}
