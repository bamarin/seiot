/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
package it.unibo.iot;

import it.unibo.iot.serial.Serial;
import it.unibo.iot.serial.SerialService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.Stage;
import jssc.SerialPortException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Main class, entry point application.
 */
public class SmartCoffeeMachine extends Application {

    private static final double WIDTH = 720;
    private static final double HEIGHT = 980;
    private final Serial serial = SerialService.getInstance();
    private static final Logger LOGGER = Logger.getLogger(SmartCoffeeMachine.class.getName());

    /**
     * Start the GUI engine, setup the environment.
     * @param primaryStage the primary stage.
     * @throws Exception exception on load fxml files.
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {
        final Parent rootPane = FXMLLoader.load(getClass().getResource("/fxml/SmartCoffeeView.fxml"));
        final Scene scene = new Scene(rootPane, WIDTH, HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();

        List<String> serialPorts = Arrays.asList(serial.getInterfacesName());
        final ChoiceDialog<String> dialog = new ChoiceDialog<>("Select", serialPorts);
        dialog.setTitle("Serial Port");
        dialog.setHeaderText("Serial Port selection");
        dialog.setContentText("Choose the Arduino port:");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(serial::setSerialPort);
        LOGGER.log(Level.INFO, "Port: " + serial.getCurrentPortName() + " selected!");

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                serial.getSerialPort().removeEventListener();
                serial.getSerialPort().closePort();
                LOGGER.log(Level.INFO, "Close serial port");
            } catch (SerialPortException ex) {
                LOGGER.log(Level.SEVERE, ex.toString(), ex);
            }
        }));

        Thread.sleep(1000);

        serial.run();
    }

    /**
     * Main method, launch JavaFX app.
     * @param args CLI args.
     */
    public static void main(final String... args) {
        launch(args);
    }
}
