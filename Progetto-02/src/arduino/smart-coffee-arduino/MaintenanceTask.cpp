/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/MaintenanceTask.h"

MaintenanceTask::MaintenanceTask(const State *currentState) {
  this->currentState = currentState;
  this->isFirstRun = true;
}

void MaintenanceTask::initTask(const int period) {
  Task::initTask(period);
}

void MaintenanceTask::tick() {
  if (*(this->currentState) == State::MAINTENANCE) {
    onBegin();
    char recByte = Serial.read();
    if (recByte == 0x01) { /* Check the return code */
      Task::messageManager.log("Char read correctly");
      Task::messageManager.sendMessage("Coffee refill: " + String(MAX_COFFEE));
      this->isFirstRun = true;
      *(this->currentState) = State::STANDBY;
    }
  }
}

void MaintenanceTask::onBegin() {
  if (this->isFirstRun) {
    Task::messageManager.log("Maintenance mode");
    Task::messageManager.setMaintenance(0x04);
    this->isFirstRun = false;
  }
}
