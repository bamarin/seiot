/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/Led.h"

Led::Led(const int pin) {
  this->pin = pin;
  pinMode(this->pin, OUTPUT);
}

void Led::switchOn() {
  digitalWrite(this->pin, HIGH);
}

void Led::switchOff() {
  digitalWrite(this->pin, LOW);
}

bool Led::isOn() {
  return digitalRead(this->pin) == HIGH ? true : false;
}
