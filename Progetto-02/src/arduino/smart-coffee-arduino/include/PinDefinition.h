/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef PIN_DEFINITION
#define PIN_DEFINITION


/* Pin defintion */
#define TRIG_PIN  4
#define ECHO_PIN  5
#define PIR_PIN   2
#define BTN       3
#define LED1      9
#define LED2      10
#define LED3      11
#define SUGAR     A0

/* Time defintion all time are in milliseconds */
#define DT1   1000
#define DT2A  5000
#define DT2B  5000
#define DT3   3000
#define DT4   5000

/* Distance definition all distance are in cm */
#define DIST1 30
#define DIST2 10

#define MAX_COFFEE  2

#endif /* end of include guard: PIN_DEFINITION */
