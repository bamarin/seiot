/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __MESSAGE_MANAGER__
#define __MESSAGE_MANAGER__

#include "Arduino.h"

class MessageManager {
public:
  void sendMessage(String message);
  void sendSugar(int quantity);
  void sendProgress(int progress);
  void setMaintenance(int code);
  void log(String messageLog);
  static MessageManager& getInstance();
private:
  MessageManager();
  static const byte MESSAGE     = 0x01;
  static const byte SUGAR       = 0x02;
  static const byte PROGRESS    = 0x03;
  static const byte MAINTENANCE = 0x04;
  static const byte LOG         = 0x09;
};

#endif /* end of include guard: __MESSAGE_MANAGER__ */
