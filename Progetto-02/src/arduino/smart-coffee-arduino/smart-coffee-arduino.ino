#include "include/PinDefinition.h"
#include "include/Task.h"
#include "include/TaskScheduler.h"
#include "include/DetectPresenceTask.h"
#include "include/OnTask.h"
#include "include/State.h"
#include "include/ReadyTask.h"
#include "include/MakeCoffeeTask.h"
#include "include/MaintenanceTask.h"
#include "include/MessageManager.h"

State currentState = State::STANDBY;
HCSR04 *sonar = new HCSR04(TRIG_PIN, ECHO_PIN);
TaskScheduler &scheduler = TaskScheduler::getInstance();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(PIR_PIN, INPUT);
  pinMode(BTN, INPUT);
  
  scheduler.initScheduler(100);

  Task *motionTask = new DetectPresenceTask(new Pir(PIR_PIN), &currentState);
  motionTask->initTask(200);
  Task *onTask = new OnTask(sonar, &currentState);
  onTask->initTask(500);
  Task *readyTask = new ReadyTask(sonar, SUGAR, new Button(BTN), &currentState);
  readyTask->initTask(100);
  Task *makeCoffeeTask = new MakeCoffeeTask(new Led(LED1), new Led(LED2), new Led(LED3), sonar, MAX_COFFEE, &currentState);
  makeCoffeeTask->initTask(500);
  Task *maintenanceTask = new MaintenanceTask(&currentState);
  maintenanceTask->initTask(200);

  scheduler.addTask(motionTask);
  scheduler.addTask(onTask);
  scheduler.addTask(readyTask);
  scheduler.addTask(makeCoffeeTask);
  scheduler.addTask(maintenanceTask);
}

void loop() {
  // put your main code here, to run repeatedly:
  scheduler.schedule();
}
