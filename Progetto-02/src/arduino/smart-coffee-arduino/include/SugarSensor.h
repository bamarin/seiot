#ifndef __SUGAR_SENSOR__
#define __SUGAR_SENSOR__

#include "Arduino.h"

class SugarSensor {
public:
  SugarSensor(const int pin);
  int getSugarQuantity();
private:
  int pin;
};


#endif /* end of include guard: __SUGAR_SENSOR__ */
