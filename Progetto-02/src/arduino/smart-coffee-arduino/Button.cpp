/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/Button.h"


Button::Button(const int pin) {
  this->pin = pin;
  pinMode(this->pin, INPUT);
}

bool Button::isPressed() {
  return digitalRead(this->pin) == HIGH ? true : false;
}
