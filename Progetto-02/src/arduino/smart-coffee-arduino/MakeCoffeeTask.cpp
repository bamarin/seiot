/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/MakeCoffeeTask.h"

MakeCoffeeTask::MakeCoffeeTask(const Led *led1, const Led *led2, const Led *led3, const HCSR04 *sonar, const int maxCoffee, const State *currentState) {
  this->leds[0] = led1;
  this->leds[1] = led2;
  this->leds[2] = led3;
  this->sonar = sonar;
  this->currentState = currentState;
  this->currentTimeout = 0;
  this->isFirstRun = true;
  this->ledPeriod = 0;
  this->ledState = 0;
  this->maxCoffee = maxCoffee;
  this->madeCoffee = 0;
}

void MakeCoffeeTask::initTask(const int taskPeriod) {
  Task::initTask(taskPeriod);
}

void MakeCoffeeTask::tick() {
  if (*(this->currentState) == State::MAKE_COFFEE) {
    onBegin();
    this->ledPeriod += Task::taskPeriod;

    if (this->ledState >= 3) {
      this->currentTimeout += Task::taskPeriod;
      this->leds[this->ledState - 1]->switchOff();
    }

    if (this->madeCoffee == this->maxCoffee && this->ledState >= 3) {
      Task::messageManager.log("Finish coffee");
      *(this->currentState) = State::MAINTENANCE;
      this->madeCoffee = 0;
      this->currentTimeout = 0;
      this->ledState = 0;
      this->ledPeriod = 0;
      this->isFirstRun = true;
    } else if ((this->sonar->dist() <= DIST2 && this->ledState >= 3) || this->currentTimeout == DT4) {
      *(this->currentState) = State::READY;
      this->currentTimeout = 0;
      this->ledState = 0;
      this->ledPeriod = 0;
      this->isFirstRun = true;
      Task::messageManager.sendProgress(0);
    }

    if (this->ledPeriod >= DT3 / 3 && this->ledState < 3) {
      this->ledPeriod = 0;
      if (this->ledState != 0) {
        this->leds[this->ledState - 1]->switchOff();
      }
      this->leds[this->ledState]->switchOn();
      this->ledState++;
      Task::messageManager.sendProgress(this->ledState);
    }
  }
}

void MakeCoffeeTask::onBegin() {
  if (this->isFirstRun) {
    this->madeCoffee++; /* increment coffe made */
    Task::messageManager.sendMessage("Making a Coffee");
    Task::messageManager.log("Makecoffe state");
    this->isFirstRun = false;
  }
}
