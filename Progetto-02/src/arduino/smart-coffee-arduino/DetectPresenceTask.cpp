/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/DetectPresenceTask.h"


DetectPresenceTask::DetectPresenceTask(const Pir *pir, const State *currentState) {
  this->currentState = currentState;
  this->pir = pir;
  this->isFirstRun = true;
}

void DetectPresenceTask::initTask(const int period) {
  Task::initTask(period);
}

void DetectPresenceTask::tick() {
  /* Execute this task until a presence is detected */
  if (*(this->currentState) == State::STANDBY) {
    onBegin(); /* execute only once */
    if (pir->isMoved()) {
      *(this->currentState) = State::ON;
      Task::messageManager.log("Move Detected");
      this->isFirstRun = true;
    }
  }
}

void DetectPresenceTask::onBegin() {
  if (this->isFirstRun) {
    Task::messageManager.log("Standby state...");
    //messageManager.sendMessage("Standby");
    this->isFirstRun = false;
  }
}
