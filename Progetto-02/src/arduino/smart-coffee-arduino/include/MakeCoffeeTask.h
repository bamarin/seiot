/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __MAKE_COFFEE_TASK__
#define __MAKE_COFFEE_TASK__

#include "Task.h"
#include "State.h"
#include "Led.h"
#ifndef __HCSR04__
#define __HCSR04__
#include <HCSR04.h>
#endif
#include "MessageManager.h"
#include "PinDefinition.h"

class MakeCoffeeTask : public Task {
public:
  MakeCoffeeTask(const Led *led1, const Led *led2, const Led *led3, const HCSR04 *sonar, const int maxCoffee, const State *currentState);
  void initTask(const int period);
  void tick();

private:
  void onBegin();
  bool isFirstRun;
  State *currentState;
  Led *leds[3];
  HCSR04 *sonar;
  int maxCoffee;
  int madeCoffee;
  int ledState;
  int currentTimeout;
  int ledPeriod;
  //MessageManager& messageManager = MessageManager::getInstance();
};

#endif /* end of include guard: __MAKE_COFFEE_TASK__ */
