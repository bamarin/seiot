/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __LED__
#define __LED__

#include "Arduino.h"

class Led {
public:
  Led(const int pin);
  void switchOn();
  void switchOff();
  bool isOn();
private:
  int pin;
};

#endif /* end of include guard: __LED__ */
