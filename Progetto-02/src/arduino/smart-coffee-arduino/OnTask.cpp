/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/OnTask.h"

OnTask::OnTask(const HCSR04 *sonar, const State *currentState) {
  this->sonar = sonar;
  this->currentState = currentState;
  this->presenceTime = 0;
  this->currentTimeout = 0;
}

void OnTask::initTask(const int period) {
  Task::initTask(period);
}

void OnTask::tick(){
  if (*(this->currentState) == State::ON) {
    onBegin();
    /* check if the user is distant < 30cm to the machine */
    if (this->sonar->dist() <= DIST1) {
      this->presenceTime += Task::taskPeriod;
      /* check if the user is in front of the machine after 1s */
      if (this->presenceTime == DT1 && this->sonar->dist() <= DIST1) {
        this->presenceTime = 0;
        this->currentTimeout = 0;
        this->isFirstRun = true;
        *(this->currentState) = State::READY;
      }
    } else { /* the user is not distant < 30cm */
      this->presenceTime = 0;
      this->currentTimeout += Task::taskPeriod;
    }

    /* check timout: go back to STANDBY */
    if (this->currentTimeout >= DT2A) {
      this->isFirstRun = true;
      this->currentTimeout = 0;
      this->presenceTime = 0;
      *(this->currentState) = State::STANDBY; //go to STANDBY
    }
  }
}

void OnTask::onBegin() {
  if (this->isFirstRun) {
    this->isFirstRun = false;
    Task::messageManager.sendMessage("Welcome!");
    Task::messageManager.log("On task");
  }
}
