/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
package it.unibo.iot;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.unibo.iot.serial.SerialService;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;

/**
 * Gui controller.
 */
public class SmartCoffeeController {

    @FXML
    private Text messages;
    @FXML
    private Button refill;
    @FXML
    private ProgressBar progress;
    @FXML
    private ProgressBar sugar;
    private final SerialService serialService = SerialService.getInstance();


    /**
     * Setup all handler and gui.
     */
    public void initialize() {
        refill.setDisable(true);
        sugar.setProgress(0.5);
        progress.setProgress(0.0);
        final Disposable messageDisposable = serialService.getMessageObservable()
                .observeOn(Schedulers.newThread())
                .subscribe(messages::setText);
        final Disposable sugarDisposable = serialService.getSugarObservable()
                .observeOn(Schedulers.newThread())
                .subscribe(val -> sugar.setProgress(val / 100.0));
        final Disposable progressDisposable = serialService.getProgressObservable()
                .observeOn(Schedulers.newThread())
                .subscribe(val -> progress.setProgress(val / 3.0));
        final Disposable maintenanceDisposable = serialService.getMaintenanceObservable()
                .observeOn(Schedulers.newThread())
                .subscribe(e -> refill.setDisable(false));

        final CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.addAll(messageDisposable, sugarDisposable, progressDisposable, maintenanceDisposable);
    }

    /**
     * Event handler for refill button.
     */
    public void refillHandler() {
        byte[] mess = { 0x01 }; // ACK
        serialService.writeOnSerial(mess);
        refill.setDisable(true);
    }
}
