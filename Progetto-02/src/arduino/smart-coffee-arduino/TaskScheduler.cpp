#include "include/TaskScheduler.h"

volatile bool timerFlag = false;

void timerHandler() {
  timerFlag = true;
}

TaskScheduler::TaskScheduler() { }

void TaskScheduler::initScheduler(const unsigned long basePeriod) {
  this->basePeriod = basePeriod;
  Timer1.initialize(basePeriod * 1000);
  Timer1.attachInterrupt(timerHandler);
  this->availableTasks = 0;
}

bool TaskScheduler::addTask(const Task *task) {
  if (this->availableTasks < MAX_TASKS - 1) {
    this->taskList[this->availableTasks] = task;
    this->availableTasks++;
    return true;
  } else {
    return false;
  }
}

void TaskScheduler::schedule() {
  while (!timerFlag);
  timerFlag = false;
  for (char i = 0; i < this->availableTasks; i++) {
    if (this->taskList[i]->isExecutable(this->basePeriod)) {
      this->taskList[i]->tick();
    }
  }
}

TaskScheduler& TaskScheduler::getInstance() {
  static TaskScheduler scheduler;
  return scheduler;
}
