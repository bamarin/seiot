/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __MAINTENANCE_TASK__
#define __MAINTENANCE_TASK__

#include "Arduino.h"
#include "Task.h"
#include "State.h"
#include "MessageManager.h"
#include "PinDefinition.h"


class MaintenanceTask : public Task {
public:
  MaintenanceTask(const State *currentState);
  void initTask(const int period);
  void tick();
private:
  void onBegin();
  bool isFirstRun;
  State *currentState;
  //MessageManager& messageManager = MessageManager::getInstance();
};

#endif /* end of include guard: __MAINTENANCE_TASK__ */
