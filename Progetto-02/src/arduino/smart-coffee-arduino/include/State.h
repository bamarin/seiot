#ifndef __STATE__
#define __STATE__

enum class State {
  STANDBY,
  ON,
  READY,
  MAKE_COFFEE,
  MAINTENANCE
};

#endif /* end of include guard: __STATE__ */
