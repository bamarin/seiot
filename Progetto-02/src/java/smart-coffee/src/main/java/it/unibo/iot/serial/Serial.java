/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
package it.unibo.iot.serial;

import io.reactivex.subjects.PublishSubject;
import jssc.SerialPort;

/**
 * Interface to model the serial RS232.
 */
public interface Serial {
    /**
     * Method to setup and run the serial.
     */
    void run();

    /**
     * Write bytes on the serial.
     * @param messages the messages to the serial.
     */
    void writeOnSerial(byte[] messages);

    /**
     * Find out all the available serial port.
     * @return the available serial port.
     */
    String[] getInterfacesName();

    /**
     * Set the serial port must be used for communication.
     * @param portName the port name to use.
     */
    void setSerialPort(String portName);

    /**
     * Get the current port in use for communication.
     * @return the current port.
     */
    String getCurrentPortName();

    /**
     * Get the observable for message.
     * @return publish subject emit messages.
     */
    PublishSubject<String> getMessageObservable();

    /**
     * Get the observable for sugar quantity.
     * @return the publish subject emit sugar quantity.
     */
    PublishSubject<Integer> getSugarObservable();

    /**
     * Get the observable for progress.
     * @return progress for make coffee.
     */
    PublishSubject<Integer> getProgressObservable();

    /**
     * Get the observable for maintenance.
     * @return maintenance code.
     */
    PublishSubject<Integer> getMaintenanceObservable();

    /**
     * Get the serial port.
     * @return the serial port.
     */
    SerialPort getSerialPort();
}
