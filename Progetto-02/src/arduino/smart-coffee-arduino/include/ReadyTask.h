/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __READY_TASK__
#define __READY_TASK__

#include "State.h"
#include "Button.h"
#ifndef __HCSR04__
#define __HCSR04__
#include <HCSR04.h>
#endif
#include "Task.h"
#include "PinDefinition.h"
#include "MessageManager.h"
#include "SugarSensor.h"

class ReadyTask : public Task {
public:
  ReadyTask(const HCSR04 *sonar, const int sugarPin, const Button *button, const State *currentState);
  void initTask(const int period);
  void tick();
private:
  void onBegin();
  bool isFirstRun;
  HCSR04 *sonar;
  Button *button;
  State *currentState;
  SugarSensor *sugarSensor;
  int currentTimeout;
  //MessageManager& messageManager = MessageManager::getInstance();
};

#endif /* end of include guard: __READY_TASK__ */
