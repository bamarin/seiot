/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/Pir.h"

Pir::Pir(const int pin) {
  this->pin = pin;
  pinMode(pin, INPUT);
}

bool Pir::isMoved() {
  return digitalRead(pin);
}
