/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
package it.unibo.iot.serial;

import io.reactivex.subjects.PublishSubject;
import jssc.*;

import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Serial manager, used to setup the serial, read and write on it.
 */
public final class SerialService implements Serial, SerialPortEventListener {
    private static SerialService instance = null;
    private static final Logger LOGGER = Logger.getLogger(SerialService.class.getName());
    private final PublishSubject<String> messagesObservable = PublishSubject.create();
    private final PublishSubject<Integer> sugarObservable = PublishSubject.create();
    private final PublishSubject<Integer> progressObservable = PublishSubject.create();
    private final PublishSubject<Integer> maintenanceObservable = PublishSubject.create();
    private final StringBuilder stringBuilder = new StringBuilder();
    private final BlockingQueue<String> queue = new LinkedBlockingQueue<>();
    private SerialPort serialPort;

    private SerialService() { }

    /**
     * Return the instance class.
     *
     * @return instance class.
     */
    public static synchronized SerialService getInstance() {
        if (instance == null) {
            instance = new SerialService();
        }
        return instance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        try {
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);
            serialPort.addEventListener(this, SerialPort.MASK_RXCHAR);
        } catch (SerialPortException ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeOnSerial(final byte[] messages) {
        try {
            serialPort.writeBytes(messages);
        } catch (SerialPortException ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getInterfacesName() {
        return SerialPortList.getPortNames();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSerialPort(final String portName) {
        this.serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
        } catch (SerialPortException ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCurrentPortName() {
        return serialPort.getPortName();
    }

    /**
     * {@inheritDoc}
     * Code legend: 0x01 <b>messages</b>
     * 0x02 <b>sugar qty</b>
     * 0x03 <b>progress on make coffee</b>
     * 0x04 <b>maintenance code</b>
     */
    @Override
    public void serialEvent(final SerialPortEvent serialPortEvent) {
        if (serialPortEvent.isRXCHAR() && serialPortEvent.getEventValue() > 0) {
            try {
                byte[] receivedData = serialPort.readBytes();

                for (final byte character : receivedData) {
                    if (character == '\n' && stringBuilder.length() > 0) {
                        queue.put(stringBuilder.toString());
                        stringBuilder.setLength(0);
                    } else {
                        stringBuilder.appendCodePoint(character);
                    }
                }

                final Iterator<String> queueIterator = queue.iterator();

                while (queueIterator.hasNext()) {
                    String message = queueIterator.next();
                    switch (message.charAt(0)) {
                        case '1':
                            messagesObservable.onNext(message.substring(1));
                            break;
                        case '2':
                            sugarObservable.onNext(Integer.parseInt(message.substring(1)));
                            break;
                        case '3':
                            progressObservable.onNext(Integer.parseInt(message.substring(1)));
                            break;
                        case '4':
                            maintenanceObservable.onNext(0x04);
                            messagesObservable.onNext("No more coffee. Waiting for recharge");
                            break;
                        case '9':
                            LOGGER.log(Level.INFO, message.substring(1));
                            break;
                        default:
                            LOGGER.log(Level.WARNING, "Case not found, wrong code received: " + message);
                    }
                    queueIterator.remove();
                }

            } catch (SerialPortException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, ex.toString(), ex);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublishSubject<String> getMessageObservable() {
        return messagesObservable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublishSubject<Integer> getSugarObservable() {
        return sugarObservable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublishSubject<Integer> getProgressObservable() {
        return progressObservable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublishSubject<Integer> getMaintenanceObservable() {
        return maintenanceObservable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SerialPort getSerialPort() {
        return serialPort;
    }
}
