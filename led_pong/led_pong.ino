/* Author: Leonardo Marini
 * Project1 - Led Pong
 * Sistemi Embedded & Internet of Things
 * A.A.: 2018/2019
 */

#include <TimerOne.h>
#include <stdlib.h>

#define BTN_1 2
#define BTN_2 3
#define BTN_3 4
#define POT A0
#define LED_1 6
#define LED_2 9
#define LED_3 10
#define FLASH 5

//Global variables:
int leds[] = {LED_1, LED_2, LED_3};	//Array containing the number of pin for each led.
int pos, dir, speed;                //Variables containing info about the ball.
int RT;					                    //Reaction Time
boolean hit = false;	              //Tells wheter the ball was hit or not.
boolean play = false;	//Tells if the game is over or being played.
int winner, shots = 0;	//End game statistics.
//The following variables are used before the game starts, to make the led fade in/out.
boolean flagState = false;
int currIntensity = 0;
int fadeAmount = 5;
int t;

//Gradually changes the brightness of the FLASH led
void pulse(){
  analogWrite(FLASH, currIntensity);
  currIntensity += fadeAmount;
  if (currIntensity <=0 || currIntensity >= 255){
    fadeAmount = -fadeAmount;
  }
  delay(20);
}

//Ends a match
void gameOver(){
  Serial.print("Game Over - The Winner is Player ");
  Serial.print(winner);
  Serial.print(" after ");
  Serial.print(shots);
  Serial.println(" shots");
  play = false;
}

//Moves the ball on timer interrupt
void moveBall(){
  if(pos != 1 && !hit)  //A player failed to hit the ball
    gameOver();
  else{
    digitalWrite(leds[pos],LOW);
	pos+=dir;
    digitalWrite(leds[pos],HIGH);
	hit = false;
  }
}

void hits(int player){
    hit = true;
    dir = -dir;
	winner = player;	//Last player who hit the ball: Player1.
	shots++;
	RT=RT/8;			//Make the ball go faster
    Timer1.setPeriod(RT);
}

//Player1 action
void actionA(){
  if(pos == 0)	//Successfully hits the ball.
    hits(1);
  else			//The player didn't have ball and tried to hit it.
    gameOver();
}

//Player2 action
void actionB(){
  if(pos == 2)	//Successfully hits the ball.
    hits(2);
  else			//The player didn't have ball and tried to hit it.
    gameOver();
}

void setup() {
  //set pins mode
  pinMode(BTN_1, INPUT);
  pinMode(BTN_2, INPUT);
  pinMode(BTN_3, INPUT);
  pinMode(POT, INPUT);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
  pinMode(FLASH, OUTPUT);
  Serial.begin(9600);
  //init RNG
  randomSeed(analogRead(POT));
  //init Timer
  Timer1.initialize(1000000);
  //define interrupts
  attachInterrupt(digitalPinToInterrupt(BTN_1), actionA, RISING);
  attachInterrupt(digitalPinToInterrupt(BTN_2), actionB, RISING);
  Timer1.attachInterrupt(moveBall);
}
void loop() {
  Timer1.stop();
  int i;
  (random(0,2))? dir= 1: dir= -1;
  //turn off all the pins representing the ball
  for(i=0;i<sizeof(leds);i++)
    digitalWrite(leds[i], LOW);
  //Ready
  Serial.println("Welcome to Led Pong. Press Key T3 to Start");
  while(!play){
	speed = analogRead(POT);	//returns a value between 0 and 1023
	if (digitalRead(BTN_3) == HIGH)
	  play = true;
	pulse();
  }
  digitalWrite(FLASH, LOW);		//Turn off flash led
  //Map the speed from a range 0-1023 to 1-10
  speed = 1 + ((10 - 1) / (1023 - 0)) * (speed);
  RT = 1000000 / speed;
  Timer1.setPeriod(RT);
  Serial.println("Go!");
  //Match starts
  pos=1;						//Ball in the middle
  digitalWrite(leds[pos],HIGH);
  delay(1000000);
  Timer1.start();
  while(play){}
}
