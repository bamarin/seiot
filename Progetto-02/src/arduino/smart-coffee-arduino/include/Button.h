/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __BUTTON__
#define __BUTTON__


#include "Arduino.h"

class Button {
public:
  Button(const int pin);
  bool isPressed();

private:
  int pin;
};


#endif /* end of include guard: __BUTTON__ */
