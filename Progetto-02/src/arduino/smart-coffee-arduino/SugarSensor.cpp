/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/SugarSensor.h"

SugarSensor::SugarSensor(const int pinMode) {
  this->pin = pinMode;
}

int SugarSensor::getSugarQuantity() {
  return (int) map(analogRead(this->pin), 0, 1023, 0, 100);
}
