#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "Task.h"
#include <TimerOne.h>

#define MAX_TASKS 15

class TaskScheduler {
  int basePeriod;
  int availableTasks;
  Task *taskList[MAX_TASKS];

public:
  void initScheduler(const unsigned long basePeriod);
  virtual bool addTask(const Task *task);
  virtual void schedule();
  static TaskScheduler& getInstance();

private:
  TaskScheduler();
}; // end class

#endif /* end of include guard: __SCHEDULER__ */
