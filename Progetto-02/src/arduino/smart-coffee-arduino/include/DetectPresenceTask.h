/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __DETECT_PRESENCE_TASK__
#define __DETECT_PRESENCE_TASK__


#include "Task.h"
#include "Pir.h"
#include "MessageManager.h"

class DetectPresenceTask : public Task {
public:
  DetectPresenceTask(const Pir *pir, const State *currentState);
  void initTask(const int period);
  void tick();

private:
  void onBegin();
  bool isFirstRun;
  State *currentState;
  Pir *pir;
  //MessageManager& messageManager = MessageManager::getInstance();
};

#endif /* end of include guard: __DETECT_PRESENCE_TASK__ */
