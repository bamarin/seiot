#ifndef __TASK__
#define __TASK__

#include "State.h"
#include "MessageManager.h"

class Task {

protected:
  int taskPeriod;
  int timeElapsed;
  MessageManager& messageManager = MessageManager::getInstance();
public:
  virtual void initTask(const int taskPeriod) {
    this->taskPeriod = taskPeriod;
    timeElapsed = 0;
  }

  virtual bool isExecutable(const int basePeriod) {
    timeElapsed += basePeriod;
    if (timeElapsed >= taskPeriod) {
      timeElapsed = 0;
      return true;
    } else {
      return false;
    }
  }

  virtual void tick() = 0;
}; // end class

#endif /* end of include guard: __TASK__ */
