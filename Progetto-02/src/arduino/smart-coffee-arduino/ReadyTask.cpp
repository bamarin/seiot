/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#include "include/ReadyTask.h"

ReadyTask::ReadyTask(const HCSR04 *sonar, const int sugarPin, const Button *button, const State *currentState) {
  this->button = button;
  this->currentState = currentState;
  this->sonar = sonar;
  this->currentTimeout = 0;
  this->isFirstRun = true;
  this->sugarSensor = new SugarSensor(sugarPin);
}

void ReadyTask::initTask(const int period) {
  Task::initTask(period);
}

void ReadyTask::tick() {
  if (*(this->currentState) == State::READY) {
    onBegin();
    Task::messageManager.sendSugar(this->sugarSensor->getSugarQuantity());
    if (this->sonar->dist() < DIST1) {
      this->currentTimeout = 0;
    } else {
      this->currentTimeout += Task::taskPeriod;
    }

    if (this->button->isPressed()) {
      Task::messageManager.log("Button pressed");
      this->isFirstRun = true;
      *(this->currentState) = State::MAKE_COFFEE;
    }

    if (this->currentTimeout >= DT2B) {
      this->currentTimeout = 0;
      this->isFirstRun = true;
      *(this->currentState) = State::ON;
    }
  }
}

void ReadyTask::onBegin() {
  if (this->isFirstRun) {
    this->isFirstRun = false;
    Task::messageManager.sendMessage("Press the button to make coffee");
    Task::messageManager.log("Ready task");
  }
}
