/**
 *	Smart Coffee Machine
 *  @author Nicolas Farabegoli	788928
 *  @author Leonardo Marini		789067
 */
#ifndef __ON_TASK__
#define __ON_TASK__

#include "Task.h"
#include "PinDefinition.h"
#include "State.h"
#include "MessageManager.h"
#ifndef __HCSR04__
#define __HCSR04__
#include <HCSR04.h>
#endif

class OnTask : public Task {

public:
  OnTask(const HCSR04 *sonar, const State *currentState);
  void initTask(const int period);
  void tick();

private:
  void onBegin();
  bool isFirstRun;
  HCSR04 *sonar;
  int currentTimeout;
  int presenceTime;
  State *currentState;
  //MessageManager& messageManager = MessageManager::getInstance();
};

#endif /* end of include guard: __ON_TASK__ */
