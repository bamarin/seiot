# IoT Domande Orale

## 0 Parte zero - Embedded

### Caratteristiche Sistema **Embedded**
      - Funzionalità specifica
	  - Risorse limitate
	  - Progettazione orientata all'efficienza
	  - Affidabilità (dependability, reliability, availability)
	  - Reattività e real-time
	    - hard real-time
	  
### Architettura Hardware
	 (Software applicativo e Sistema operativo sono opzionali)
	  - CPU (general/single-purpose , MCU (arduino e ESP), SoC()
	    - single-purpose
		- MCU (arduino e ESP)
		- SoC (raspberry)
	  - ROM
	  - RAM
	  - Sensori
	  - Attuatori
	  - Circuito esterno
	  - Interfaccia di comunicazione (seriale)
	  
## 1 Prima parte - Arduino

### 1.1. Caratteristiche **MicroControllore** (CPU, Memorie, Arch. Hardware)
      - MicroController (ATmega328)
	  - I/O, RAM, ROM...
	  Tutto su un'unica scheda per rendere il sistema autosufficiente
	
	
## 3 Terza parte - SoC

### 3.1 Caratteristiche sistemi real time
### 3.2 Altri sistemi operativi oltre a Raspian
### 3.3 Caratteristiche di un Soc
      Sistema comp[leto incorporato in un unico chip